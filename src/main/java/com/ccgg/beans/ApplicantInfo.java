package com.ccgg.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ccgg_applicant_info")
public class ApplicantInfo {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
	@SequenceGenerator(name = "SEQ", sequenceName = "CCGG_APPLICANT_INFO_SEQ")
	private int id;
	@Column
	private String name;
	@Column 
	private int age;
	@Column
	private String phone_number;
	@Column
	private String email;
	@Column
	private String resume;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ApplicantInfo [id=" + id + ", name=" + name + ", age=" + age + ", phone_number=" + phone_number
				+ ", email=" + email + ", resume=" + resume + "]";
	}
	public ApplicantInfo(int id, String name, int age, String phone_number, String email, String resume) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.phone_number = phone_number;
		this.email = email;
		this.resume = resume;
	}
	public ApplicantInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}

