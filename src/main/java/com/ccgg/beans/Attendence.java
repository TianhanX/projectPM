package com.ccgg.beans;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ccgg.attendence")
public class Attendence {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
	@SequenceGenerator(name = "SEQ", sequenceName = "CCGG_ATTENDENCE_SEQ")
	private int id;
	@Column(unique = true)
	private Timestamp in_time;
	@Column(unique = true)
	private Timestamp out_time;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	@JsonIgnore
	User user;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getIn_time() {
		return in_time;
	}
	public void setIn_time(Timestamp in_time) {
		this.in_time = in_time;
	}
	public Timestamp getOut_time() {
		return out_time;
	}
	public void setOut_time(Timestamp out_time) {
		this.out_time = out_time;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		return "Attendence [id=" + id + ", in_time=" + in_time + ", out_time=" + out_time
				+ ", user=" + user + "]";
	}
	
	public Attendence(int id, Timestamp in_time, Timestamp out_time, User user) {
		super();
		this.id = id;
		this.in_time = in_time;
		this.out_time = out_time;
		this.user = user;
	}
	public Attendence() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}

