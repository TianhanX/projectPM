package com.ccgg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.Employee;
import com.ccgg.http.Response;
import com.ccgg.service.EmployeeService;

@RestController()
@RequestMapping("/employees")
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;
	
	@GetMapping
	public List<Employee> getEmployees() {
		return employeeService.getEmployees();
	}
	
	@PostMapping
	public Response addEmployees(@RequestBody Employee employee) {
		return employeeService.addEmployees(employee);
	}
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@PutMapping
	public Response updateEmployees(@RequestBody Employee employee) {
		return employeeService.updateEmployees(employee);
	}
	
	@PreAuthorize("hasAuthority(ROLE_ADMIN)")
	@DeleteMapping("/{id}")
	public Response deleteEmployees(@PathVariable int id) {
		return employeeService.deleteEmployees(id);
	}
}

