package com.ccgg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.ApplicantInfo;
import com.ccgg.http.Response;
import com.ccgg.service.ApplicantInfoService;

@RestController()
@RequestMapping("/applicants")
//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_HR', 'ROLE_AP')")
public class ApplicantController {
	@Autowired
	ApplicantInfoService applicantInfoService;
	
	@GetMapping
	public List<ApplicantInfo> getApplicantInfo() {
		return applicantInfoService.getApplicantInfo();
	}
	
	@PostMapping
	public Response addApplicantInfo(@RequestBody ApplicantInfo applicantInfo) {
		return applicantInfoService.addApplicantInfo(applicantInfo);
	}
	
	@PutMapping
	public Response updateApplicantInfo(@RequestBody ApplicantInfo applicantInfo) {
		return applicantInfoService.updateApplicantInfo(applicantInfo);
	}
	
	@DeleteMapping("/{id}")
	public Response deleteApplicantInfo(@PathVariable int id) {
		return applicantInfoService.deleteApplicantInfo(id);
	}
}
