package com.ccgg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.Department;
import com.ccgg.http.Response;
import com.ccgg.service.DepartmentService;

@RestController()
@RequestMapping("/departments")
//@PreAuthorize("hasAuthority(ROLE_ADMIN)")
public class DepartmentController {
	@Autowired
	DepartmentService departmentService;
	
	@GetMapping
	public List<Department> getDepartment() {
		return departmentService.getDepartment();
	}
	
	@PostMapping
	public Response addDepartment(@RequestBody Department department) {
		return departmentService.addDepartment(department);
	}
	
	@PutMapping
	public Response updateDepartment(@RequestBody Department department) {
		return departmentService.updateDepartment(department);
	}
	
	@DeleteMapping("/{id}")
	public Response deleteDepartment(@PathVariable int id) {
		return departmentService.deleteDepartment(id);
	}
}

