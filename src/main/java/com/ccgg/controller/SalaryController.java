package com.ccgg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.Salary;
import com.ccgg.http.Response;
import com.ccgg.service.SalaryService;

@RestController()
@RequestMapping("/salary")
public class SalaryController {
	@Autowired
	SalaryService salaryService;
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping
	public List<Salary> getSalary() {
		return salaryService.getSalary();
	}
	
	@PreAuthorize("hasAuthority(ROLE_ADMIN)")
	@PutMapping
	public Response updateSalary(@RequestBody Salary salary) {
		return salaryService.updateSalary(salary);
	}
}

