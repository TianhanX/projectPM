package com.ccgg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.ApplicantInfo;;

@Repository
public interface ApplicantInfoDao extends JpaRepository<ApplicantInfo, Integer>{
	ApplicantInfo findById(int id);
}
