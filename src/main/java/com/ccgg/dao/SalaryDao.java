package com.ccgg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.Salary;

@Repository
public interface SalaryDao extends JpaRepository<Salary, Integer>{
	Salary findById(int id);
}

