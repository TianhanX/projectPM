package com.ccgg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.Department;

@Repository
public interface DepartmentDao extends JpaRepository<Department, Integer>{
	Department findById(int id);
}

