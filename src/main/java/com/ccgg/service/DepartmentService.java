package com.ccgg.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccgg.beans.Department;
import com.ccgg.dao.DepartmentDao;
import com.ccgg.http.Response;

@Service
@Transactional
public class DepartmentService {
	@Autowired
	DepartmentDao departmentDao;
	
	public List<Department> getDepartment() {
		return departmentDao.findAll();
	}
	
	public Response addDepartment(Department department) {
		
		departmentDao.save(department);
		
		return new Response(true);
	}
	
	public Response updateDepartment(Department department) {
		Department d = departmentDao.findById(department.getId());
		
		d.setDepartment_name(department.getDepartment_name());
	
		departmentDao.save(department);
		
		return new Response(true);
	}
	
	public Response deleteDepartment(int id) {
		if (departmentDao.findById(id) != null) {
			departmentDao.deleteById(id);
			return new Response(true);
		} else {
			return new Response(false, "Department is not found!");
		}
	}
}
