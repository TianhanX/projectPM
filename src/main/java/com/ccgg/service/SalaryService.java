package com.ccgg.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccgg.beans.Salary;
import com.ccgg.dao.SalaryDao;
import com.ccgg.http.Response;

@Service
@Transactional
public class SalaryService {
	@Autowired
	SalaryDao salaryDao;
	
	public List<Salary> getSalary() {
		return salaryDao.findAll();
	}
	
	public Response addSalary(Salary salary) {
		
		salaryDao.save(salary);
		
		return new Response(true);
	}
	
	public Response updateSalary(Salary salary) {
		Salary s = salaryDao.findById(salary.getId());
		
		s.setSalary(s.getSalary());
		
		salaryDao.save(salary);
		
		return new Response(true);
	}
	
	public Response deleteSalary(int id) {
		if (salaryDao.findById(id) != null) {
			salaryDao.deleteById(id);
			return new Response(true);
		} else {
			return new Response(false, "Salary is not found!");
		}
	}
}
