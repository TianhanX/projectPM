package com.ccgg.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccgg.beans.ApplicantInfo;
import com.ccgg.dao.ApplicantInfoDao;
import com.ccgg.http.Response;

@Service
@Transactional
public class ApplicantInfoService {
	@Autowired
	ApplicantInfoDao applicantInfoDao;
	
	public List<ApplicantInfo> getApplicantInfo() {
		return applicantInfoDao.findAll();
	}
	
	public Response addApplicantInfo(ApplicantInfo applicantInfo) {
		
		applicantInfoDao.save(applicantInfo);
		
		return new Response(true);
	}
	
	public Response updateApplicantInfo(ApplicantInfo applicantInfo) {
		ApplicantInfo a = applicantInfoDao.findById(applicantInfo.getId());
		
		a.setName(applicantInfo.getName());
		a.setAge(applicantInfo.getAge());
		a.setPhone_number(applicantInfo.getPhone_number());
		a.setEmail(applicantInfo.getEmail());
		a.setResume(applicantInfo.getResume());
		
		applicantInfoDao.save(applicantInfo);
		
		return new Response(true);
	}
	
	public Response deleteApplicantInfo(int id) {
		if (applicantInfoDao.findById(id) != null) {
			applicantInfoDao.deleteById(id);
			return new Response(true);
		} else {
			return new Response(false, "Applicant is not found!");
		}
	}
}
