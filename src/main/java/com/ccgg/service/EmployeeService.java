package com.ccgg.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccgg.beans.Employee;
import com.ccgg.dao.EmployeeDao;
import com.ccgg.http.Response;

@Service
@Transactional
public class EmployeeService {
	@Autowired
	EmployeeDao employeeDao;
	
	public List<Employee> getEmployees() {
		return employeeDao.findAll();
	}
	
	public Response addEmployees(Employee employee) {
		
		employeeDao.save(employee);
		
		return new Response(true);
	}
	
	public Response updateEmployees(Employee employee) {
		Employee e = employeeDao.findById(employee.getId());
		
		e.setName(employee.getName());
		e.setAge(employee.getAge());
		e.setPhone_number(employee.getPhone_number());
		e.setEmail(employee.getEmail());
		e.setJob_title(employee.getJob_title());
		
		employeeDao.save(employee);
		
		return new Response(true);
	}
	
	public Response deleteEmployees(int id) {
		if (employeeDao.findById(id) != null) {
			employeeDao.deleteById(id);
			return new Response(true);
		} else {
			return new Response(false, "Employee is not found!");
		}
	}
}

